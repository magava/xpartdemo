package model;

import javafx.application.HostServices;

import java.sql.Connection;

public class LoginToItemsData {
    private HostServices hostServices;
    private Connection connection;

    public LoginToItemsData() {
    }

    public LoginToItemsData(HostServices hostServices, Connection connection) {
        this.hostServices = hostServices;
        this.connection = connection;
    }

    public HostServices getHostServices() {
        return hostServices;
    }

    public void setHostServices(HostServices hostServices) {
        this.hostServices = hostServices;
    }

    public Connection getConnection() {
        return connection;
    }

    public void setConnection(Connection connection) {
        this.connection = connection;
    }
}
