package model;

import javafx.scene.control.RadioButton;

import java.util.Objects;

public class CategoryInfo implements Comparable{
    private RadioButton catRadioBtn;
    private String name;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CategoryInfo)) return false;
        CategoryInfo that = (CategoryInfo) o;
        return Objects.equals(getName(), that.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName());
    }

    public CategoryInfo() {
    }

    public CategoryInfo(RadioButton catRadioBtn, String name) {
        this.catRadioBtn = catRadioBtn;
        this.name = name;
    }

    public RadioButton getCatRadioBtn() {
        return catRadioBtn;
    }

    public void setCatRadioBtn(RadioButton catRadioBtn) {
        this.catRadioBtn = catRadioBtn;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    @Override
    public int compareTo(Object o) {
        CategoryInfo obj = (CategoryInfo)o;
        return this.name.compareTo(obj.name);
    }
}
