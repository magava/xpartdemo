package model;

public class YearEntity implements Entity {
    private String fromYear;
    private String toYear;

    public YearEntity() {
    }

    public YearEntity(String fromYear, String toYear) {
        this.fromYear = fromYear;
        this.toYear = toYear;
    }

    public String getFromYear() {
        return fromYear;
    }

    public void setFromYear(String fromYear) {
        this.fromYear = fromYear;
    }

    public String getToYear() {
        return toYear;
    }

    public void setToYear(String toYear) {
        this.toYear = toYear;
    }

    @Override
    public String getEntityName() {
        return fromYear + " - " + toYear;
    }
}
