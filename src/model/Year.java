package model;

public class Year implements Entity {
    private Integer year;

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Year() {
    }

    public Year(Integer year) {
        this.year = year;
    }

    @Override
    public String getEntityName() {
        return String.valueOf(year);
    }
}
