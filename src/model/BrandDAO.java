package model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class BrandDAO {

    static public ObservableList<Brand> getBrands(Connection conn) {
        ObservableList<Brand> modelNamesList = FXCollections.observableArrayList();

        String sql = "select id, model from cat_to_brand";


        try (PreparedStatement ps = conn.prepareStatement(sql);
             ResultSet rs = ps.executeQuery()){

            while ( rs.next() )
            {
                modelNamesList.add(new Brand(rs.getInt("id"),rs.getString("model")));
            }


        } catch (SQLException e1) {
            e1.printStackTrace();
        }

        return modelNamesList;
    }


    static public ObservableList<Model> getModelsByBrandId(Connection conn, int id) {
        ObservableList<Model> modelNamesList = FXCollections.observableArrayList();

        String sql = "select f1, f3 from cat_to_model_test where f2=" + "'" + id +"'";

        try ( PreparedStatement ps = conn.prepareStatement(sql);
              ResultSet rs = ps.executeQuery())
        {
            while ( rs.next() )
            {
                modelNamesList.add(new Model(rs.getInt("f1"),String.valueOf(id),rs.getString("f3")));
            }

        } catch (SQLException e1) {
            e1.printStackTrace();
        }

        return modelNamesList;

    }

}
