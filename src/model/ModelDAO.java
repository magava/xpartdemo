package model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ModelDAO {

    static public ObservableList<Modification> getModifsByModelId(Connection conn, int id) {
        ObservableList<Modification> modifsList = FXCollections.observableArrayList();

        String sql = "select f1,f3,f4,f5 from cat_to_modif_test where f2=" + id;

        try (
                PreparedStatement ps = conn.prepareStatement(sql);
                ResultSet rs = ps.executeQuery()) {
            while (rs.next()) {
                modifsList.add(new Modification(rs.getInt("f1"), String.valueOf(id), rs.getString("f3"), rs.getString("f4"), rs.getString("f5")));
            }


        } catch (SQLException e1) {
            e1.printStackTrace();
        }

        return modifsList;

    }


}
