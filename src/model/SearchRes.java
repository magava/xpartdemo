package model;

import javafx.scene.control.Hyperlink;

public class SearchRes {

    private String name;
    private String qty;
    private String info;
    private Hyperlink link;


    public SearchRes() {
    }

    public SearchRes(String name, String qty, String info, Hyperlink link) {
        this.name = name;
        this.qty = qty;
        this.info = info;
        this.link = link;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public Hyperlink getLink() {
        return link;
    }

    public void setLink(Hyperlink link) {
        this.link = link;
    }
}
