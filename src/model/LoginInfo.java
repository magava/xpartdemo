package model;

public class LoginInfo {

    private String url;
    private String userName;
    private String password;
    private String databaseName;

    public LoginInfo() {

    }

    public LoginInfo(String url, String userName, String password, String databaseName) {
        this.url = url;
        this.userName = userName;
        this.password = password;
        this.databaseName = databaseName;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDatabaseName() {
        return databaseName;
    }

    public void setDatabaseName(String databaseName) {
        this.databaseName = databaseName;
    }
}
