package sample;

import connectivity.ConnectByLoginInfo;
import javafx.application.HostServices;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import model.LoginInfo;
import model.LoginToItemsData;

import java.io.IOException;
import java.sql.Connection;

public class LoginController {

    @FXML
    TextField url;

    @FXML
    TextField loginName;

    @FXML
    TextField loginPassword;

    @FXML
    TextField loginDatabase;

    private HostServices hostServices;
    private ConnectByLoginInfo connectByLoginInfo;
    private Connection connection;
    private LoginToItemsData loginToItemsData;

    public LoginController(){
    }

    public HostServices getHostServices() {
        return hostServices;
    }

    public void setHostServices(HostServices hostServices) {
        this.hostServices = hostServices;
    }


    public Connection getConnection(LoginInfo info) {
        connectByLoginInfo = new ConnectByLoginInfo();
        return connectByLoginInfo.getConnection(info);
    }

    public Connection getConnection(){
        return connection;
    }

    public LoginToItemsData getLoginToItemsData() {
        return loginToItemsData;
    }

    public void changeSceneWhenConnected(ActionEvent event) throws IOException {
        try {
            LoginInfo loginInfo = new LoginInfo();
            loginInfo.setUrl(url.getText());
            loginInfo.setDatabaseName(loginDatabase.getText());
            loginInfo.setUserName(loginName.getText());
            loginInfo.setPassword(loginPassword.getText());

            connection = getConnection(loginInfo);

            if (connection != null) {

                loginToItemsData = new LoginToItemsData();
                loginToItemsData.setConnection(connection);
                loginToItemsData.setHostServices(getHostServices());

                FXMLLoader loader = new FXMLLoader();
                loader.setLocation(getClass().getResource("items.fxml"));

                Parent root = (Parent) loader.load();

                ItemsController itemsController = loader.getController();
                itemsController.setLoginData(loginToItemsData);

                Scene scene = new Scene(root);
                Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                stage.setScene(scene);

                stage.show();

            } else {
                System.out.println("Try again...Cannot Connect.");
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setContentText("Не удалось соединиться с сервером. Введите верные данные.");
                alert.show();
            }

        }catch (IOException e){
            e.printStackTrace();
            System.out.println("Wrong data.....");
        }

        //Get Stage info
        /*Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        window.setScene(sampleScene);
        window.show();*/
    }

    public void initialize()
    {

    }
}
