package sample;

import javafx.application.HostServices;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.util.StringConverter;
import model.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class ItemsController {

    @FXML
    ComboBox brandCombo;

    @FXML
    ComboBox modelCombo;

    @FXML
    ComboBox modifCombo;

    @FXML
    ComboBox yearCombo;

    @FXML
    TableView searchTbl;

    @FXML
    TableColumn<SearchRes, String> nameCol;

    @FXML
    TableColumn<SearchRes, String> qtyCol;

    @FXML
    TableColumn<SearchRes, String> infoCol;

    @FXML
    TableColumn<SearchRes, Hyperlink> linkCol;

    @FXML
    VBox rbtnVbox;

    ToggleGroup radioGroup = new ToggleGroup();

    private String selectedCategory;
    private HostServices hostServices;
    private Connection conn;

    ObservableList<SearchRes> oldSearchResList;
    ObservableList<RadioButton> allCategoriesRadioButtons = FXCollections.observableArrayList();

    public ItemsController() {
    }

    public void setLoginData(LoginToItemsData loginDataInfo) {
        hostServices = loginDataInfo.getHostServices();
        conn = loginDataInfo.getConnection();

        initSearchTable();
        initBrandCombo();
        initModelCombo();
        initCategories();
    }


    public String getSelectedCategory() {
        return selectedCategory;
    }

    public void setSelectedCategory(String selectedCategory) {
        this.selectedCategory = selectedCategory;
    }

    public HostServices getHostServices() {
        return hostServices;
    }

    public void setHostServices(HostServices hostServices) {
        this.hostServices = hostServices;
    }

    public void initBrandCombo() {
        ObservableList<Brand> modelNamesList = BrandDAO.getBrands(conn);
        brandCombo.setItems(modelNamesList);
        renderCombo(brandCombo);

        modelCombo.setDisable(true);
        yearCombo.setDisable(true);
        modifCombo.setDisable(true);

        brandCombo.setOnAction((event) -> {
            modifCombo.setValue(null);
            yearCombo.setValue(null);
            modelCombo.setDisable(false);
            if (brandCombo.getSelectionModel().getSelectedItem() != null) {
                int brandId = ((Brand) (brandCombo.getSelectionModel().getSelectedItem())).getId();
                initModelComboByBrand(brandId);
            }
        });
    }

    public void initModelComboByBrand(int brandId) {
        ObservableList<Model> modelNamesList = BrandDAO.getModelsByBrandId(conn, brandId);
        modelCombo.setItems(modelNamesList);
        renderCombo(modelCombo);
    }

    public void initModelCombo() {
        modelCombo.setOnAction((event) -> {

            if (modelCombo.getValue() != null) {
                modifCombo.setDisable(false);
                yearCombo.setDisable(false);
                int modelId = ((Model) (modelCombo.getSelectionModel().getSelectedItem())).getF1();
                initModifComboByModelId(modelId);

            } else {
                yearCombo.setDisable(true);
                modifCombo.setDisable(true);
            }
        });
    }

    public void initModifComboByModelId(int modelId) {
        ObservableList<Modification> modifsList = ModelDAO.getModifsByModelId(conn, modelId);
        ObservableList<YearEntity> yearEntities = FXCollections.observableArrayList();

        List<String> startYears = new ArrayList<>();
        List<String> endYears = new ArrayList<>();

        for (Modification modification : modifsList) {
            String startYear = modification.getF3();
            String endYear = modification.getF4();
            if (startYear == null || startYear.equals("") || startYear.equals(" "))
                startYear = String.valueOf(LocalDate.now().getYear());
            if (endYear == null || endYear.equals("") || endYear.equals(" "))
                endYear = String.valueOf(LocalDate.now().getYear());

            yearEntities.add(new YearEntity(startYear, endYear));

            startYears.add(startYear);
            endYears.add(endYear);
        }

        String minStart = Collections.min(startYears);
        String maxEnd = Collections.max(endYears);

        int min = Integer.valueOf(minStart);
        int max = Integer.valueOf(maxEnd);

        List<Integer> res = IntStream.rangeClosed(min, max).boxed().collect(Collectors.toList());
        ObservableList<Year> yearObservable = FXCollections.observableList(
                res.stream().map((x) -> new Year(x)).collect(Collectors.toList()));

        yearCombo.setItems(yearObservable);
        renderCombo(yearCombo);

        modifCombo.setItems(modifsList);
        renderCombo(modifCombo);
    }

    public void renderCombo(ComboBox myComboBox) {
        // Define rendering of the list of values in ComboBox drop down.
        myComboBox.setCellFactory((comboBox) -> {
            return new ListCell<Entity>() {
                @Override
                protected void updateItem(Entity item, boolean empty) {
                    super.updateItem(item, empty);

                    if (item == null || empty) {
                        setText(null);
                    } else {
                        setText(item.getEntityName());
                    }
                }
            };
        });

        // Define rendering of selected value shown in ComboBox.
        myComboBox.setConverter(new StringConverter<Entity>() {
            @Override
            public String toString(Entity entity) {
                if (entity == null) {
                    return null;
                } else {
                    return entity.getEntityName();
                }
            }

            @Override
            public Entity fromString(String personString) {
                return null; // No conversion fromString needed.
            }
        });

    }

    public void initSearchTable() {
        searchTbl.setStyle("-fx-font-size: 14 ;");
        nameCol.setCellValueFactory(new PropertyValueFactory<SearchRes, String>("name"));
        nameCol.setCellFactory(param -> {
            TableCell<SearchRes, String> cell = new TableCell<>();
            Text text = new Text();
            cell.setGraphic(text);

            cell.setPrefHeight(Control.USE_COMPUTED_SIZE);
            text.textProperty().bind(cell.itemProperty());
            text.wrappingWidthProperty().bind(cell.widthProperty());
            return cell;
        });

        qtyCol.setCellValueFactory(new PropertyValueFactory<SearchRes, String>("qty"));
        infoCol.setCellValueFactory(new PropertyValueFactory<SearchRes, String>("info"));
        linkCol.setCellValueFactory(new PropertyValueFactory<SearchRes, Hyperlink>("link"));
    }

    public ObservableList<SearchRes> getOldSearchResList() {
        return oldSearchResList;
    }

    public void setOldSearchResList(ObservableList<SearchRes> oldSearchResList) {
        this.oldSearchResList = oldSearchResList;
    }

    public void searchByCombo(ActionEvent e) {
        ObservableList<SearchRes> searchResList = FXCollections.observableArrayList();
        ObservableList<CategoryInfo> radioButtons = FXCollections.observableArrayList();

        String query = getCategoryQueryString(e);
        try {
            PreparedStatement ps = conn.prepareStatement(query);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                initByYear(rs, searchResList, radioButtons);
            }

            radioButtons = FXCollections.observableList(radioButtons.stream().distinct().sorted().collect(Collectors.toList()));

            searchTbl.setItems(null);
            setOldSearchResList(searchResList);
            searchTbl.setItems(searchResList);

            RadioButton rb = (RadioButton) radioGroup.getSelectedToggle();
            if (rb != null) {
                selectedCategory = rb.getText();
                rb.setSelected(false);
            } else {
                selectedCategory = "";
            }

            rbtnVbox.getChildren().clear();
            for (CategoryInfo x : radioButtons) {
                rbtnVbox.getChildren().add(x.getCatRadioBtn());
            }
        } catch (SQLException e1) {
            e1.printStackTrace();
        }

    }

    public void initByYear(ResultSet rs, ObservableList<SearchRes> searchResList, ObservableList<CategoryInfo> radioButtons) throws SQLException {
        String catYears = rs.getString("Years");
        String[] yearsArray = catYears.split(" - ");

        int from = Integer.parseInt(yearsArray[0]);
        int to = LocalDate.now().getYear();

        if (yearsArray.length == 2) {
            to = Integer.parseInt(yearsArray[1]);
        }

        if ((yearCombo.getValue() != null &&
                ((Year) yearCombo.getValue()).getYear() >= from &&
                (((Year) yearCombo.getValue()).getYear()) <= to)
                || yearCombo.getValue() == null) {

            Hyperlink hyperlink = getHyperlink(rs.getString("Manufacturer"), rs.getString("Articul"));
            searchResList.add(new SearchRes(rs.getString("Name"), rs.getString("Qty"),
                    rs.getString("Info"), hyperlink
            ));

            if (radioButtons != null) {
                RadioButton catRadioButton = new RadioButton(rs.getString("Category"));
                catRadioButton.setToggleGroup(radioGroup);
                radioButtons.add(new CategoryInfo(catRadioButton, rs.getString("Category")));
            }
        }
    }


    public void searchByCategory(ActionEvent e) {
        String query = getCategoryQueryString(e);
        getCategories(query);
    }


    private String getCategoryQueryString(ActionEvent e) {
        Brand selectedBrand = ((Brand) (brandCombo.getSelectionModel().getSelectedItem()));
        Model selectedModel = ((Model) (modelCombo.getSelectionModel().getSelectedItem()));
        Modification selectedModif = ((Modification) (modifCombo.getSelectionModel().getSelectedItem()));

        String brand = (selectedBrand == null) ? " " : "Manufacturer=" + "'" + selectedBrand.getEntityName() + "'";
        String model = (selectedModel == null) ? " " : " and Model=" + "'" + selectedModel.getEntityName() + "'";
        String modif = (selectedModif == null) ? " " : " and Modifications=" + "'" + selectedModif.getEntityName() + "'";

        RadioButton rb = (RadioButton) radioGroup.getSelectedToggle();

        if (rb != null) {
            selectedCategory = rb.getText();
        } else {
            selectedCategory = "";
        }

        String andPart = (selectedBrand == null) && (selectedModel == null)
                && (selectedModif == null) ? " " : " and ";

        String category = (((Button) e.getSource()).getId().equals("filterComboBtn") || selectedCategory == null || selectedCategory.equals("")) ? " " : andPart + "Category=" + "'" + selectedCategory + "'";

        String wherePart = ((selectedBrand == null) && (selectedModel == null)
                && (selectedModif == null) && category.equals(" ")) ? " " : " where ";

        String query = "select distinct Name,Qty,Info,Photo,Category,Articul,Manufacturer,Years from catalog_to_test" + wherePart + brand + model + modif + category;
        return query;
    }


    public void getCategories(String query) {
        ObservableList<SearchRes> searchResList = FXCollections.observableArrayList();

        try (PreparedStatement ps = conn.prepareStatement(query);
             ResultSet rs = ps.executeQuery()
        ) {
            while (rs.next()) {
                initByYear(rs, searchResList, null);
            }
            searchTbl.setItems(null);
            searchTbl.setItems(searchResList);
        } catch (SQLException e1) {
            e1.printStackTrace();
        }
    }


    public Hyperlink getHyperlink(String manufacturer, String articul) {
        Hyperlink hyperlink = new Hyperlink("Найти в Google");
        String hyperlinkUri = "https://www.google.com/search?q=" + manufacturer + " " + articul;
        hyperlink.setOnAction((ActionEvent event) ->
                getHostServices().showDocument(hyperlinkUri));
        return hyperlink;
    }


    public void initCategories() {
        String query = "SELECT distinct f3 FROM cat_to_data_test order by f3;";

        try (PreparedStatement ps = conn.prepareStatement(query);
             ResultSet rs = ps.executeQuery()
        ) {
            while (rs.next()) {
                RadioButton catRadioButton = new RadioButton(rs.getString("f3"));
                catRadioButton.setToggleGroup(radioGroup);
                allCategoriesRadioButtons.add(catRadioButton);
                rbtnVbox.getChildren().add(catRadioButton);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteCategoryFilter(ActionEvent e) {
        RadioButton rb = (RadioButton) radioGroup.getSelectedToggle();

        if (rb != null) {
            rb.setSelected(false);
            searchTbl.setItems(getOldSearchResList());
        }
    }

    public void deleteComboFilter(ActionEvent e) {
        brandCombo.setValue(null);

        modelCombo.setValue(null);
        modelCombo.setDisable(true);

        yearCombo.setValue(null);
        yearCombo.setDisable(true);

        modifCombo.setValue(null);
        modifCombo.setDisable(true);

        searchTbl.setItems(null);

        rbtnVbox.getChildren().clear();
        if (allCategoriesRadioButtons != null) {
            for (RadioButton catRbtn : allCategoriesRadioButtons) {
                rbtnVbox.getChildren().add(catRbtn);
            }
        }
    }
}
