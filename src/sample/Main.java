package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.sql.Connection;
import java.sql.SQLException;

public class Main extends Application {

    private LoginController loginController;

    @Override
    public void start(Stage primaryStage) throws Exception{

        FXMLLoader loader = new FXMLLoader(getClass().getResource("login.fxml"));
        Parent root = loader.load();

        loginController = loader.getController();
        loginController.setHostServices(getHostServices());

        primaryStage.setTitle("Подбор запчастей Java");
        primaryStage.setScene(new Scene(root, 800, 600));

        primaryStage.show();

    }

    @Override
    public void stop() {
        Connection connection = loginController.getConnection();
        try {
            if (connection != null && !connection.isClosed()) {
                connection.close();
            }
        }catch (SQLException ex) {
            ex.printStackTrace();
        }
    }



    public static void main(String[] args) {
        launch(args);
    }
}
