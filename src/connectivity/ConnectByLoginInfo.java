package connectivity;

import model.LoginInfo;

import java.sql.Connection;
import java.sql.DriverManager;

public class ConnectByLoginInfo {
    Connection connection;

    public Connection getConnection(LoginInfo loginInfo){
        String url = loginInfo.getUrl();
        String dbName = loginInfo.getDatabaseName();
        String username = loginInfo.getUserName();
        String password = loginInfo.getPassword();

        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://" + url + "/" + dbName + "?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC", username,password);

            //connection = DriverManager.getConnection("server=127.0.0.1;uid=root;pwd=12345;database=fl");
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("CANT GET CONNECTION.......................");
        }
        /*finally
        {
            if (connection != null)
            {
                try
                {
                    connection.close ();
                    System.out.println ("Database connection terminated");
                }
                catch (Exception e) {  }
            }
        }*/

        return connection;
    }
}
